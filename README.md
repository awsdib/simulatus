# Simulatus


## 1.  Frontend App

How it looks
--------

![](images/demo.PNG)

Overview
--------
A React application visualizing map data for a ride pooling service.

Installation
------------
Clone the repo then run a simple npm install
`npm i`

Running the app
--------
**To lint the code**
`npm run lint`

**Run with mocked data**

Run the app in development mode using
`npm start`

To run the production build script
`npm run build`
You will find the resulted files in the 'dist' folder

**To run the app against the backend API please follow the steps below**

1.  Go to the file: <rootpath>/Simulatus/App/src/App.jsx
2.  Change the address of the API endpoint
3.  Change the `useMockedData` flag to false to use output from the API

Libraries and packages used
--------
*  Webpack is used as a bundling library
*  eslint for linting the project
*  Jest and Enzyme for testing
*  For the map [react-leaflet](https://react-leaflet.js.org/) is used
*  For the visualizations [chart.js](https://www.chartjs.org) is used to genereate the relevant charts


## 2.  Backend

Overview
--------
The backend API is written in Python3 and exposes a [ridepooling simulator](https://github.com/door2door-io/mi-code-challenge/)

Required Packages
--------
[Flask](https://palletsprojects.com/p/flask/) for exposing the simulator to a Rest API

Installation
--------
1.  First install Flask

Using pip:

`py -m pip install Flask`

2.  Navigate to the backend folder
3.  Create a new virtual environment using the following command in the terminal

`py -m venv venv`

4.  Activate the newly created environment by navigating to /venv/Scripts then run activate

`cd venv\Scripts`

`activate`

5. Next set flask variables in the new environment 

`set FLASK_ENV=development`

`set FLASK_APP=app.py`

6. Navigate back to the Backend directory and then run

`flask run`

7. When the flask app runs it will inform you about the endpoint of the API (Default is http://127.0.0.1:5000)

>  **Note**: In order for the flask application to work, CORS was handled by allowing all requests. In production mode this should be handled is a more secure way. 

