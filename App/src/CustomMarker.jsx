import React from 'react'
import L from 'leaflet';
import PropTypes from "prop-types";
import { Marker, Popup } from 'react-leaflet';

/** A simple function component which accepts:
 *  @param color specifies the color of the marker.
 *  @param id a unique id to identify markers.
 *  @param position an array of coordinates(latlong).
 *  @param text the text to display in the popup tooltip as description of points.
 */
function CustomMarker({color="#000", id="0", position=[0,0], text=""}) {
        const HtmlStyles = `
            background-color: ${color};
            width: 3rem;
            height: 3rem;
            display: block;
            left: -1.5rem;
            top: -1.5rem;
            position: relative;
            border-radius: 3rem 3rem 0;
            transform: rotate(45deg);
            border: 1px solid #FFFFFF`

        const icon = new L.divIcon({
            className: "my-custom-pin",
            iconAnchor: [0, 24],
            labelAnchor: [-6, 0],
            popupAnchor: [0, -36],
            html: `<span style="${HtmlStyles}" />`
        });

        return (
            <Marker  icon={icon} key={id} position={[position[0], position[1]]} >
                <Popup>
                    {text}
                </Popup>
            </Marker>
        )
}
CustomMarker.propTypes = {
    /** specifies the color of the marker. */
    color: PropTypes.string.isRequired,
    /** a unique id to identify markers. */
    id: PropTypes.string.isRequired,
    /** an array of coordinates(latlong). */
    position: PropTypes.array.isRequired,
    /** the text to display in the popup tooltip as description of points.*/
    text: PropTypes.string.isRequired
}
export default CustomMarker;