import React from 'react'
import PropTypes from "prop-types";
import { Map as LeafletMap, TileLayer } from 'react-leaflet';


import CustomMarker from './CustomMarker.jsx'

/** Colors of the 2 types of markers */
const pickupMarkerColor = '#34d23a';
const dropoffMarkerColor = '#51b0ff';

/**
 *  This component renders a map from osm.
 *  The map is initialized with default values for center and zoom properties
 */
class Map extends React.Component {
  render() {
    /**
    *  A Child component (CustomMarker) is rendered to reflect the two types of 
    *  points (pickup and dropoff).
    */
    return (
      <LeafletMap        
        center={[52.531261, 13.417568]}
        zoom={12}
        maxZoom={22}
        attributionControl={true}
        zoomControl={false}
        doubleClickZoom={true}
        scrollWheelZoom={true}
        dragging={true}
        animate={true}
        easeLinearity={0.35}>   
        <TileLayer url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'/>
        {
          this.props.pickup
          ? this.props.pickup.map(t =>
              <CustomMarker color={pickupMarkerColor} 
                  key={t.id} id={t.id} 
                  position={[t.geometry.coordinates[1], t.geometry.coordinates[0]]} 
                  text={t.properties.name} />
            )
          :""
        }
        {
            this.props.dropoff
            ? this.props.dropoff.map(t =>
              <CustomMarker color={dropoffMarkerColor} 
                  key={t.id} id={t.id} 
                  position={[t.geometry.coordinates[1], t.geometry.coordinates[0]]} 
                  text={t.properties.name} />
            )
            :""
        }
      </LeafletMap>
    );
  }
}
Map.propTypes = {
  /** Array of GeoJSON points for the pickup points passed to the Marker component. */
  pickup: PropTypes.array,
  /** Array of GeoJSON points for the dropoff points passed to the Marker component. */
  dropoff: PropTypes.array
}
export default Map