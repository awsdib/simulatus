import React, { Component } from "react";
import PropTypes from "prop-types";

import Input from "./Input.jsx";

/**
 *  This component holds a form to specify parameters for API calls.
 */
class FormContainer extends Component {
  constructor() {
    super();
    this.state = {
      /** Boundary box default values */
      min_lon: "13.34014892578125",
      min_lat: "52.52791908000258",
      max_lon: "13.506317138671875",
      max_lat: "52.562995039558004",
      /** Number of requests */
      noReq: "5"
    };
    this.handleChange = this.handleChange.bind(this);
  }

  /**
   * This function handles the user entered data and saves it in the state
   * @param event data
   */
  handleChange(event) {
    this.setState({ [event.target.id]: event.target.value });
  }

  render() {
    const { min_lon, min_lat, max_lon, max_lat, noReq } = this.state;
    return (
      <form id="simulation-form" className="container-fluid p-0">
        <div className="row">
          <div className="col-12">
            <Input
              text="Number of requests"
              label="Number of requests"
              type="text"
              id="noReq"
              value={noReq}
              handleChange={this.handleChange}
              cssClass="form-control-sm"
            />
          </div>
        </div>
        <div className="row bbox">
          <div className="col-6">
            <Input
              text="Min Lon"
              label="Min Lon"
              type="text"
              id="min_lon"
              value={min_lon}
              handleChange={this.handleChange}
              cssClass="form-control-sm"
            />
          </div>
          <div className="col-6">
            <Input
              text="Min Lat"
              label="Min Lat"
              type="text"
              id="min_lat"
              value={min_lat}
              handleChange={this.handleChange}
              cssClass="form-control-sm"
            />
          </div>
        </div>   
        <div className="row bbox">
          <div className="col-6">
            <Input
              text="Max Lon"
              label="Max Lon"
              type="text"
              id="max_lon"
              value={max_lon}
              handleChange={this.handleChange}
              cssClass="form-control-sm"
            />
          </div>
          <div className="col-6">
            <Input
              text="Max Lat"
              label="Max Lat"
              type="text"
              id="max_lat"
              value={max_lat}
              handleChange={this.handleChange}
              cssClass="form-control-sm"
            />
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <button type="button" 
                  className="btn btn-success btn-block mt-3" 
                  onClick={()=>this.props.runCallback(event, this.state.min_lon, this.state.min_lat, this.state.max_lon, max_lat, this.state.noReq)}>Run</button>
          </div>
        </div>
      </form>
    );
  }
}
FormContainer.propTypes = {
  /**
  *  This is the callback passed from parent (Sidebar) to the Button element.
  */
  runCallback: PropTypes.func
};
export default FormContainer;