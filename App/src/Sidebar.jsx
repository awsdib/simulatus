import React, { Component } from "react";
import PropTypes from "prop-types";

import FormContainer from "./FormContainer.jsx";
import ResultsContainer from "./ResultsContainer.jsx";

/**
 *  This component holds the input and output data and renders FormContainer
 *  and ResultsContainer components.
 */
class Sidebar extends Component {
  render() {
    return (
      <div>
        <div>
          <h5>Start a new simulation</h5>
          <FormContainer runCallback={this.props.runCallback}/>
        </div>
        <div className="mt-3">
          <h5>Results of simulation</h5>
          <ResultsContainer distanceValues={this.props.distanceValues} 
                        toggleStatsCallback={this.props.toggleStatsCallback} 
                        addToStats={this.props.addToStats}/>
        </div>
      </div>
    );
  }
}
Sidebar.propTypes = {
  /**
   *  This callback is passed to FormContainer to handle the changed user entered data back from
   *  the nested components.
   */
  runCallback: PropTypes.func.isRequired,
  /**
   *  The no. bookings passed to the Result component to generate the output.
   */
  distanceValues: PropTypes.array.isRequired,
  /** a callback function from App component to add the current values to compare */
  addToStats: PropTypes.func.isRequired,
  /**
   *  Callback to App to expand and collapse the statistics panel
   */
  toggleStatsCallback: PropTypes.func
};
export default Sidebar;