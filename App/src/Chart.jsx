import React, { Component } from 'react';
import PropTypes from "prop-types";
import {
  XYPlot,
  XAxis,
  YAxis,
  VerticalGridLines,
  HorizontalGridLines,
  VerticalBarSeries,
} from 'react-vis';

import '../node_modules/react-vis/dist/style.css';

/**
 *  The Chart component uses react-vis library to render a graph
 */
class Chart extends Component {
  render() {
    /**
     *  The chart component expects data in a form of [{x:"x", y:"y"}].
     *  A dynamic loop through the passed array is used to create the data source format.
     */
    var statsChart = []

    for(let i=0; i< this.props.stats.length; i++) {
      var arr = this.props.stats[i]
      arr.forEach( (e,idx)=> arr[idx].x= idx+"-"+(idx+1) )
      statsChart.push(<VerticalBarSeries className="vertical-bar-series-example" data={arr} />)
    }

    return (
      <div className="chart-container">
        <XYPlot xType="ordinal" width={400} height={500} xDistance={100}>
          <VerticalGridLines />
          <HorizontalGridLines />
          <XAxis />
          <YAxis />
          {statsChart}
        </XYPlot>
      </div>
    );
  }
}
Chart.propTypes = {
  stats: PropTypes.array
};
export default Chart;
