import React, { Component } from "react";
import ReactDOM from "react-dom";

import Sidebar from "./Sidebar.jsx";
import StatsBar from "./StatsBar.jsx";
import Map from "./Map.jsx";
import "./App.css"

/** 
*   Change the base url to your API endpoint
*/
const baseUrl = "http://127.0.0.1:5000/run/"

/**
 *  This flag indicates the use of hardcoded output from the API and 
 *  not request it from the backend endpoint
 */
const useMockedData = true ;

/**
*   The main component which prepares data from the API and 
*   renders child components (Sidebar and Map)
*/
class App extends Component {
  constructor() {
    super();
    this.state = {
      /** The nomber of booking per distance ranges */
      distanceValues : [],
      /** Most popular dropoff points */
      dropoffPoints : [],
      /** Most popular pickup points */
      PickupPoints : [],
      /** Show statistics flag */
      ShowStats: false,
      /** Statistics to compare */
      stats: []
    } 
    this.runSimulation = this.runSimulation.bind(this);
    this.toggleStatsCallback = this.toggleStatsCallback.bind(this);
    this.addToStats = this.addToStats.bind(this);
  }

/** 
 *  This function is invoked after the Run button is clicked.
 *  The function make a request to the API with the defined parameters from child components.
 *  The resulted data is saved in the state, and passed as props to child components.  
*/
  runSimulation (event, min_lon, min_lat, max_lon, max_lat, noReq) {

    if(useMockedData) {
      var Ran1 = Math.floor(Math.random() * 100) + 1 ;
      var Ran2 = Math.floor(Math.random() * 100) + 1 ;
      var Ran3 = Math.floor(Math.random() * 100) + 1 ;
      var Ran4 = Math.floor(Math.random() * 100) + 1 ;

      var text = `{
        "booking_distance_bins": {
          "From 0->1km": `+Ran1+`,
          "From 1->2km": `+Ran2+`,
          "From 2->3km": `+Ran3+`,
          "From 3->4km": `+Ran4+`
        },
        "most_popular_dropoff_points": {
          "type": "FeatureCollection",
          "features": [
            {
              "id": "1916",
              "type": "Feature",
              "properties": {
                "id": "u33e12v2pv06",
                "name": "Berlin, Arnold-Zweig-Str."
              },
              "geometry": {
                "type": "Point",
                "coordinates": [
                  13.42184948812617,
                  52.56274084126975
                ]
              }
            }
          ]
        },
        "most_popular_pickup_points": {
          "type": "FeatureCollection",
          "features": [
            {
              "id": "1214",
              "type": "Feature",
              "properties": {
                "id": "u33e02dhc078",
                "name": "Berlin, Mickestr."
              },
              "geometry": {
                "type": "Point",
                "coordinates": [
                  13.373155814668303,
                  52.5621571180536
                ]
              }
            }
          ]
        }
      }`
      let obj = JSON.parse(text);

      let distanceData = [
        { x: "0.5", y: obj.booking_distance_bins["From 0->1km"]},
        { x: "1.5", y: obj.booking_distance_bins["From 1->2km"]},
        { x: "2.5", y: obj.booking_distance_bins["From 2->3km"]},
        { x: "3.5", y: obj.booking_distance_bins["From 3->4km"]}
      ]

      const newState = Object.assign({}, this.state, {
        distanceValues: distanceData,      
        PickupPoints: obj.most_popular_pickup_points.features,
        dropoffPoints: obj.most_popular_dropoff_points.features
      })
      this.setState(newState);
    } else {
      let url = baseUrl
      + min_lon + "/"
      + min_lat + "/"
      + max_lon + "/"
      + max_lat + "/"
      + noReq;

      /**
       *  The API resturns non-valid JSON format.
       *  The format is modified with regular expression by replacing abnormal values.
       */
      fetch(url)
          .then(result=>result.text())
          .then(text => {

            text = text.replace(/'/g,"\"");
            text = text.replace(/"{/g,"{");
            text = text.replace(/}"/g,"}");
            
            let obj = JSON.parse(text);

            const newState = Object.assign({}, this.state, {
              distanceValues: [ 
                { x: "0.5", y: obj.booking_distance_bins["From 0->1km"]},
                { x: "1.5", y: obj.booking_distance_bins["From 1->2km"]},
                { x: "2.5", y: obj.booking_distance_bins["From 2->3km"]},
                { x: "3.5", y: obj.booking_distance_bins["From 3->4km"]}
              ],
              PickupPoints: obj.most_popular_pickup_points.features,
              dropoffPoints: obj.most_popular_dropoff_points.features
            })

            this.setState(newState);
          })
          .catch(error => console.log(error));
    }
  }

  /**
   *  Show or hide the statistics panel
   */
  toggleStatsCallback() {
    this.setState({ShowStats: !this.state.ShowStats})
  }

  /**
   *  This method adds the current KPI results to be compared in the statistics panel
   */
  addToStats() {
    /**
     *  For a deep clone of (this.state.distanceValues) the JSON approach is used and
     *  a new array is created then pushed into the overall statistics object (state.stats)
     */
    let newStats = this.state.stats;
    var obj = Array.from(this.state.distanceValues)
    var newArr = []
    obj.forEach(e=>  newArr.push(JSON.parse(JSON.stringify(e))))
    newStats.push(newArr)
    this.setState({stats: newStats},console.log(this.state.stats))
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="row">
            <div className="col-3 m-0 pt-4 right-shadow sidebar-container">
                <div id="new-simulation-form" className="SideBar">
                  <Sidebar runCallback={this.runSimulation} 
                          addToStats={this.addToStats} 
                          distanceValues={this.state.distanceValues} 
                          toggleStatsCallback={this.toggleStatsCallback}/>
                </div>
            </div>
            <div className="col-9 p-0">
              <div className="container m-0 p-0 map-container">
                <div className="row">  
                  <div className={(this.state.ShowStats? "col-5 right-shadow": "hidestats")+" pt-4"}>
                    <StatsBar toggleStatsCallback={this.toggleStatsCallback} 
                              stats={this.state.stats}/>
                  </div>
                  <div className="col-7 p-0">
                    <Map pickup={this.state.PickupPoints} dropoff={this.state.dropoffPoints}/>
                  </div>
                  <div className="legend">
                    <div className="legend-row">
                      <div className="lg-icon lg-icon-pickup"></div>
                      <p>Popular pickup points</p>
                    </div>
                    <div className="legend-row">
                      <div className="lg-icon lg-icon-dropoff"></div>
                      <p>Popular dropoff points</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    );
  }
}
export default App;
const wrapper = document.getElementById("app");
wrapper ? ReactDOM.render(<App />, wrapper) : false;