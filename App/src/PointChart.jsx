import React, { Component } from 'react';
import PropTypes from "prop-types";
import {
  XYPlot,
  XAxis,
  YAxis,
  VerticalGridLines,
  HorizontalGridLines,
  MarkSeries,
  Hint
} from 'react-vis';

/**
 *  The Chart component uses react-vis library to render a graph
 */
class PointChart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: null
    };
    this._forgetValue = this._forgetValue.bind(this);
    this._rememberValue = this._rememberValue.bind(this);
  }
  
  /** These two methods handle the hover behavior to show the hint tooltip */
  _forgetValue() {
    this.setState({
      value: null
    });
  }
  _rememberValue(value) {
    this.setState({value});
  }

  render() {
    /**
     *  The data the library expects is in a form of {x:"x", y:"y"}.
     *  A dynamic loop through the passed array is used to create the data source format.
     */
    const {value} = this.state;
    var data = this.props.distanceValues
    return (
      <div className="chart-container">
        <XYPlot xDomain={[0, 4]} width={300} height={200}>
          <VerticalGridLines />
          <HorizontalGridLines />
          <YAxis/>
          <XAxis tickValues={[0, 1, 2, 3, 4]}/>
          <MarkSeries
              onValueMouseOver={this._rememberValue}
              onValueMouseOut={this._forgetValue}
              className="mark-series-example"
              strokeWidth={2}
              opacity="0.8"
              size={10}
              data={data}
            />
            {value ? <Hint value={value} /> : null}
        </XYPlot>
      </div>
    );
  }
}
PointChart.propTypes = {
  distanceValues: PropTypes.array.isRequired
};
export default PointChart;
