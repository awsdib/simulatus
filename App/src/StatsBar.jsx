import React, { Component } from "react";
import PropTypes from "prop-types";
import Chart from "./Chart.jsx";

/**
 *  This component holds the input and output data and renders FormContainer
 *  and ResultsContainer components.
 */
class StatsBar extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
            <div className="col-10 stats-container">
                <h5>Statistics</h5>
                <Chart stats={this.props.stats}/>
            </div>
            <div className="expand close-icon" onClick={this.props.toggleStatsCallback}>
                ◄
            </div>
        </div>
      </div>
    );
  }
}
StatsBar.propTypes = {
  /**
   *  This callback is passed to FormContainer to handle the changed user entered data back from
   *  the nested components.
   */
  toggleStatsCallback: PropTypes.func.isRequired,
  /**
   *  The no. bookings passed to the Result component to generate the output.
   */
  stats: PropTypes.array.isRequired
};
export default StatsBar;