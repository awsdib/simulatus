import React, { Component } from "react";
import PropTypes from "prop-types";

import PointChart from "./PointChart.jsx";

/**
 *  This component holds the results of the simulation
 */
class ResultContainer extends Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div className="container-fluid p-0">
        <div className="container-fluid p-0 mb-4">
          <div className="row">
            <div className="col-6">
              <button type="button" 
                    className="btn btn-block mt-3" 
                    onClick={()=>this.props.addToStats(event)}>Add to compare</button>
            </div>
            <div className="col-6">
              <button type="button" 
                  className="btn btn-block mt-3" 
                  onClick={()=>this.props.toggleStatsCallback(event)}>View Stats</button>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <PointChart distanceValues={this.props.distanceValues}/>
          </div>
        </div>
      </div>
    );
  }
}
ResultContainer.propTypes = {
  /**
   *  The no. bookings retured is passed to the (Chart) component for the visualization
   */
  distanceValues: PropTypes.array.isRequired,
  /**
   *  Callback to App component to add the current results to compare
   */
  addToStats: PropTypes.func,
  /**
   *  Callback to App to expand and collapse the statistics panel
   */
  toggleStatsCallback: PropTypes.func
};
export default ResultContainer;