// src/Input.test.js

import React from 'react';
import ReactDOM from 'react-dom';
import Input from '../src/Input';
import { mount, shallow } from 'enzyme';

const values = {
  label: "label",
  text: "text",
  type: "type",
  id: "1",
  value: "value",
  handleChange: function(){},
  cssClass: "class"
};

describe ('<Input /> Rendering with Props', () => {
  it("Input renders correctly", () => {
    const wrapper = shallow(
      <Input />
    );
    expect(wrapper).toMatchSnapshot();
  });
  it ('accepts correct props', () => {
    const wrapper = mount(<Input text={values.text}
                                label={values.label}
                                type={values.type}
                                id={values.id}
                                value={values.value}
                                handleChange={values.handleChange}
                                cssClass={values.cssClass} />);
    expect(wrapper.props()).toEqual(values)
  })
})