// src/StatsBar.test.js

import React from 'react';
import ReactDOM from 'react-dom';
import StatsBar from '../src/StatsBar';
import { mount, shallow } from 'enzyme';

const values = [[
    { x: "0.5", y: 10},
    { x: "1.5", y: 20},
    { x: "2.5", y: 30},
    { x: "3.5", y: 40}
]]

describe ('<StatsBar />', () => {
  it("StatsBar renders correctly", () => {
      const wrapper = shallow(
        <StatsBar />
      );
      expect(wrapper).toMatchSnapshot();
  });
  it ('accepts correct props', () => {
    const wrapper = mount(<StatsBar stats={values} />);
    expect(wrapper.props().stats).toEqual(values)
  })
})


