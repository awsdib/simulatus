// src/ResultsContainer.test.js

import React from 'react';
import ReactDOM from 'react-dom';
import ResultsContainer from '../src/ResultsContainer';
import { mount, shallow } from 'enzyme';

const values = [
  { x: "0.5", y: 10},
  { x: "1.5", y: 20},
  { x: "2.5", y: 30},
  { x: "3.5", y: 40}
]



describe ('<ResultsContainer />', () => {
  it("ResultsContainer renders correctly", () => {
    const wrapper = shallow(
      <ResultsContainer />
    );
    expect(wrapper).toMatchSnapshot();
  });
  it ('accepts correct props', () => {
    const wrapper = mount(<ResultsContainer distanceValues={values} 
                                          toggleStatsCallback={function(){}} 
                                          addToStats={function(){}} />);
    expect(wrapper.props().distanceValues).toEqual(values)
  })
})