// src/CustomMarker.test.js

import React from 'react';
import ReactDOM from 'react-dom';
import CustomMarker from '../src/CustomMarker';
import { mount, shallow } from 'enzyme';

it("CustomMarker renders correctly", () => {
    const wrapper = shallow(
      <CustomMarker />
    );
    expect(wrapper).toMatchSnapshot();
});
