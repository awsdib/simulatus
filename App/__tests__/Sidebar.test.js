// src/Sidebar.test.js

import React from 'react';
import ReactDOM from 'react-dom';
import Sidebar from '../src/Sidebar';
import { mount, shallow } from 'enzyme';

const values = {
  func: function(){},
  values: [
    { x: "0.5", y: 10},
    { x: "1.5", y: 20},
    { x: "2.5", y: 30},
    { x: "3.5", y: 40}
  ]
}

describe ('<Sidebar />', () => {
  it("Sidebar renders correctly", () => {
      const wrapper = shallow(
        <Sidebar />
      );
      expect(wrapper).toMatchSnapshot();
  });
  it ('accepts correct props', () => {
    const wrapper = mount(<Sidebar runCallback={values.func} distanceValues={values.values} />);
    expect(wrapper.props().distanceValues).toEqual(values.values)
    expect(wrapper.props().runCallback).toEqual(values.func)
  })
})


