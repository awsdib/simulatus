// src/Map.test.js

import React from 'react';
import ReactDOM from 'react-dom';
import Map from '../src/Map';
import { mount, shallow } from 'enzyme';

const values = [{
  "id": "1214",
  "type": "Feature",
  "properties": {
    "id": "u33e02dhc078",
    "name": "Berlin, Mickestr."
  },
  "geometry": {
    "type": "Point",
    "coordinates": [
      13.373155814668303,
      52.5621571180536
    ]
  }
}]

describe ('<Map />', () => {
  it("Map renders correctly", () => {
    const wrapper = shallow(
      <Map />
    );
    expect(wrapper).toMatchSnapshot();
  });
  it ('accepts correct props', () => {
    const wrapper = mount(<Map pickup={values} dropoff={values} />);
    expect(wrapper.props().pickup).toEqual(values)
    expect(wrapper.props().dropoff).toEqual(values)
  })
})