// src/Chart.test.js

import React from 'react';
import ReactDOM from 'react-dom';
import Chart from '../src/Chart';
import { mount, shallow } from 'enzyme';

const values = [[
  { x: "0.5", y: 10},
  { x: "1.5", y: 20},
  { x: "2.5", y: 30},
  { x: "3.5", y: 40}
]]

describe ('<Chart /> Rendering with Props', () => {
  it("Chart renders correctly", () => {
    const wrapper = shallow(
      <Chart stats={values}/>
    );
    expect(wrapper).toMatchSnapshot();
  });
  it ('accepts correct props', () => {
    const wrapper = mount(<Chart stats={values} />);
    expect(wrapper.props().stats[0]).toEqual(values[0])
  })
})