// src/PointChart.test.js

import React from 'react';
import ReactDOM from 'react-dom';
import PointChart from '../src/PointChart';
import { mount, shallow } from 'enzyme';

const values = [
  { x: "0.5", y: 10},
  { x: "1.5", y: 20},
  { x: "2.5", y: 30},
  { x: "3.5", y: 40}
]

describe ('<PointChart /> Rendering with Props', () => {
  it("PointChart renders correctly", () => {
    const wrapper = shallow(
      <PointChart distanceValues={values}/>
    );
    expect(wrapper).toMatchSnapshot();
  });
  it ('accepts correct props', () => {
    const wrapper = mount(<PointChart distanceValues={values} />);
    expect(wrapper.props().distanceValues).toEqual(values)
  })
})