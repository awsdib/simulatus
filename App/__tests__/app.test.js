// src/App.test.js

import React from 'react';
import ReactDOM from 'react-dom';
import App from '../src/App';
import { mount, shallow } from 'enzyme';

it("App renders correctly", () => {
    const wrapper = shallow(
      <App />
    );
    expect(wrapper).toMatchSnapshot();
});