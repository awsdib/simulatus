// src/FormContainer.test.js

import React from 'react';
import ReactDOM from 'react-dom';
import FormContainer from '../src/FormContainer';
import { mount, shallow } from 'enzyme';

const values = function() {}

describe ('<FormContainer /> Rendering with Props', () => {
  it("FormContainer renders correctly", () => {
    const wrapper = shallow(
      <FormContainer />
    );
    expect(wrapper).toMatchSnapshot();
  });
  it ('accepts correct props', () => {
    const wrapper = mount(<FormContainer runCallback={values} />);
    expect(wrapper.props().runCallback).toEqual(values)
  })
})