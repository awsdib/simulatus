from simulator import Simulator
from flask import Flask
app = Flask(__name__)

@app.route('/')
def index():
  return 'Server Works!'
  
@app.route('/run/<float:minLon>/<float:minLat>/<float:maxLon>/<float:maxLat>/<int:noReq>')
def say_hello(minLon, minLat, maxLon, maxLat, noReq):
    bounding_box = (minLon, minLat, maxLon, maxLat)
    result = Simulator(bounding_box).simulate(1)
    return str(result)

@app.after_request # blueprint can also be app~~
def after_request(response):
    header = response.headers
    header['Access-Control-Allow-Origin'] = '*'
    return response

